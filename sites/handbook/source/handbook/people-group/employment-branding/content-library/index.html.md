---
layout: handbook-page-toc
title: "Content library - life at GitLab"
---

# Welcome to the life at GitLab content library

This content library is a curated list of blog posts, articles, videos, awards, and quick facts that help tell the story of life at GitLab. 

# How to use the content library

Whether you're on the Recruiting team or another team at GitLab, we want all team members to have the information they need to spread the word about GitLab and what it's like to work here. 

Use this content library as a resource whenever you're acting as a talent ambassador for GitLab: Sharing on social media, having conversations with people in your network, speaking at an event, etc. 

# Quick facts about life at GitLab 



# Articles



# Blogs

# Videos

# Awards



# Messaging

# Contribute to the content library 